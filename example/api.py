from rest_framework import viewsets, routers, permissions, filters
from .models import Language, Snippet

router = routers.DefaultRouter()


class LanguageViewSet(viewsets.ModelViewSet):
    model = Language
   
router.register(r'language', LanguageViewSet)   

class SinppetViewSet(viewsets.ModelViewSet):
    model = Snippet
    filter_fields = ('lang',)
    filter_backends = (filters.DjangoFilterBackend,)

router.register(r'snippet', SinppetViewSet)
