from django.db import models

# Create your models here.


class Language(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return str(self.id)

class Snippet(models.Model):
    lang = models.ForeignKey(Language)
    content = models.TextField()

    def __unicode__(self):
        return str(self.id)


