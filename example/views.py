# Create your views here.

import django_filters
from django.shortcuts import render_to_response
from .models import Language, Snippet


class SnippetFilter(django_filters.FilterSet):
    class Meta:
        model = Snippet
        fields = ['lang']
        
def snippet_list(request):
    f = SnippetFilter(request.GET, queryset = Snippet.objects.all())
    return render_to_response('example/view.html',{'filter':f})
