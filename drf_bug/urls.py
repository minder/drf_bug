from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from example.api import router

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'drf_bug.views.home', name='home'),
    # url(r'^drf_bug/', include('drf_bug.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^snippets/$', 'example.views.snippet_list'),
)
